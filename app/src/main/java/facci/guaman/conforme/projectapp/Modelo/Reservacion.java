package facci.guaman.conforme.projectapp.Modelo;

public class Reservacion {

    private String nombre, direccion, telefono, fecha, hora, codigo, cantidad, observacion, calificacion, calificacionAdmin, idCliente;

    public Reservacion() {
    }

    public Reservacion(String nombre, String direccion, String telefono, String fecha, String hora, String codigo, String cantidad, String observacion, String calificacion, String calificacionAdmin, String idCliente) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.fecha = fecha;
        this.hora = hora;
        this.codigo = codigo;
        this.cantidad = cantidad;
        this.observacion = observacion;
        this.calificacion = calificacion;
        this.calificacionAdmin = calificacionAdmin;
        this.idCliente = idCliente;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getCalificacionAdmin() {
        return calificacionAdmin;
    }

    public void setCalificacionAdmin(String calificacionAdmin) {
        this.calificacionAdmin = calificacionAdmin;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }
}
