package facci.guaman.conforme.projectapp.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.FadingCircle;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import facci.guaman.conforme.projectapp.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MenuDetalle extends AppCompatActivity {

    private TextView detalleNombre, detallePrecio, detalleDescripcion;
    private ImageView detalleFoto;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private Button eliminar;
    private ProgressBar progressBar;
    private String merriweatherBold, merriweatherBoldItalic;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_detalle);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Merriweather-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());


        merriweatherBold = "fonts/Merriweather-Bold.ttf";
        merriweatherBoldItalic = "fonts/Merriweather-BoldItalic.ttf";

        detalleNombre = (TextView) findViewById(R.id.detalleNombre);
        detallePrecio = (TextView) findViewById(R.id.detallePrice);
        eliminar =(Button) findViewById(R.id.EliminarMenu);

        detalleNombre.setTypeface(Typeface.createFromAsset(this.getAssets(), merriweatherBold));
        eliminar.setTypeface(Typeface.createFromAsset(this.getAssets(), merriweatherBoldItalic));



        detalleDescripcion = (TextView) findViewById(R.id.detalleDescription);
        detalleFoto = (ImageView) findViewById(R.id.detalleFoto);

        progressBar = (ProgressBar)findViewById(R.id.spin_kit2);
        FadingCircle fadingCircle = new FadingCircle();
        progressBar.setIndeterminateDrawable(fadingCircle);

        if (getIntent().getStringExtra("tipoUser").equals("Admin")){
            eliminar.setVisibility(View.VISIBLE);
        }

        detalleNombre.setText(getIntent().getStringExtra("Nombre"));
        detallePrecio.setText(getIntent().getStringExtra("Precio"));
        detalleDescripcion.setText(getIntent().getStringExtra("Descripcion"));

        Picasso.with(this).load(getIntent().getStringExtra("urlFoto")).into(detalleFoto);
        collapsingToolbarLayout = findViewById(R.id.collapsing);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppbar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapseAppbar);
        collapsingToolbarLayout.setTitle(getIntent().getStringExtra("Nombre"));

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("MENU");
                databaseReference.child(getIntent().getStringExtra("idMenu")).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(MenuDetalle.this, "MENU ELIMINADO", Toast.LENGTH_SHORT).show();
                        finish();
                        startActivity(new Intent(MenuDetalle.this, PropietarioMenu.class));
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
            }
        });

    }
}
