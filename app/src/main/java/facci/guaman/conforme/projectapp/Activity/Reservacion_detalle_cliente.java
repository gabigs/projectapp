package facci.guaman.conforme.projectapp.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

import java.util.HashMap;

import facci.guaman.conforme.projectapp.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Reservacion_detalle_cliente extends AppCompatActivity {

    private EditText codigo, fecha, hora, personas, observacion;
    private TextView titulo, subTituloCliente, subTituloAdmin, subTituloDatoCliente;
    private Button calificar;
    private CardView cardView;
    private RatingBar ratingBar, ratingBarAdmin;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference, usuario, databaseReferenceR;
    private String userId, codigoReserva, merriweatherBold, merriweatherBoldItalic;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Merriweather-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_reservacion_detalle);

        setTitle("Detalle reservacion");

        merriweatherBold = "fonts/Merriweather-Bold.ttf";
        merriweatherBoldItalic = "fonts/Merriweather-BoldItalic.ttf";

        titulo = (TextView) findViewById(R.id.tituloDetalleReserva);
        subTituloCliente = (TextView) findViewById(R.id.subTituloCliente);
        subTituloAdmin = (TextView) findViewById(R.id.subTituloAdmin);
        subTituloDatoCliente = (TextView) findViewById(R.id.subTituloDatoCliente);
        calificar = (Button) findViewById(R.id.btnCalificar);

        titulo.setTypeface(Typeface.createFromAsset(this.getAssets(), merriweatherBold));
        subTituloCliente.setTypeface(Typeface.createFromAsset(this.getAssets(), merriweatherBold));
        subTituloAdmin.setTypeface(Typeface.createFromAsset(this.getAssets(), merriweatherBold));
        subTituloDatoCliente.setTypeface(Typeface.createFromAsset(this.getAssets(), merriweatherBold));
        calificar.setTypeface(Typeface.createFromAsset(this.getAssets(), merriweatherBoldItalic));

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        userId = user.getUid();
        usuario = firebaseDatabase.getReference();
        databaseReference = firebaseDatabase.getReference("USUARIOS");
        databaseReferenceR = firebaseDatabase.getReference("RESERVACIONES");


        cardView = (CardView) findViewById(R.id.datosCliente);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingBarAdmin = (RatingBar) findViewById(R.id.ratingBarAdmin);

        if(getIntent().getStringExtra("tipo").equals("Cliente")){
            cardView.setVisibility(View.GONE);
        }


        if(getIntent().getStringExtra("calificacion").equals("0")){
            calificar.setVisibility(View.VISIBLE);
            ratingBar.setVisibility(View.GONE);
        }else{
            calificar.setVisibility(View.GONE);
            ratingBar.setVisibility(View.VISIBLE);
        }

        calificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calificarCliente();
            }
        });

        codigoReserva = getIntent().getStringExtra("codigo");

        codigo = (EditText) findViewById(R.id.txtCodigoReservacion);
        fecha = (EditText) findViewById(R.id.txtFechaReservacion);
        hora = (EditText) findViewById(R.id.txtHoraReservacion);
        personas = (EditText) findViewById(R.id.txtPersonaReservacion);
        observacion = (EditText) findViewById(R.id.txtOnservacionReservacion);

        codigo.setText(codigoReserva);
        fecha.setText(getIntent().getStringExtra("fecha"));
        hora.setText(getIntent().getStringExtra("hora"));
        personas.setText(getIntent().getStringExtra("persona"));
        observacion.setText(getIntent().getStringExtra("observacion"));
        ratingBar.setRating(Float.parseFloat(getIntent().getStringExtra("calificacion")));
        ratingBarAdmin.setRating(Float.parseFloat(getIntent().getStringExtra("caliAdmin")));
    }

    private void calificarCliente(){
        final AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
        dialogo.setTitle("Califique su atencion");
        dialogo.setCancelable(false);
        LayoutInflater inflater = LayoutInflater.from(this);
        View calificacion = inflater.inflate(R.layout.layout_new_calificacion, null);
        final RatingBar ratingBar = calificacion.findViewById(R.id.ratingBarGeneral);
        dialogo.setView(calificacion);
        dialogo.setPositiveButton("Calificar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String cali = String.valueOf(ratingBar.getRating());
                HashMap<String, Object> objectHashMap = new HashMap<>();
                objectHashMap.put("calificacion", cali);
                databaseReference.child(userId).child("RESERVACIONES").child(codigoReserva).updateChildren(objectHashMap);
                databaseReferenceR.child(codigoReserva).updateChildren(objectHashMap);
            }
        });
        dialogo.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogo.show();

    }


    }
