package facci.guaman.conforme.projectapp.Activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.FadingCircle;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

import facci.guaman.conforme.projectapp.Clases.IncioSesionActivo;
import facci.guaman.conforme.projectapp.Fragment.fragemento_reservacion;
import facci.guaman.conforme.projectapp.Fragment.fragmento_menu;
import facci.guaman.conforme.projectapp.Fragment.fragmento_perfil;
import facci.guaman.conforme.projectapp.Modelo.Reservacion;
import facci.guaman.conforme.projectapp.Modelo.Usuario;
import facci.guaman.conforme.projectapp.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BarMenu extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private String userId, nombreUsuario, nombreUsuario1, direccion, telefono;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private IncioSesionActivo incioSesionActivo;
    private ViewPager mViewPager;
    private  int dia,mes,ano,hora,minutos;
    private ProgressBar progressBar;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_menu);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Merriweather-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressBar = (ProgressBar)findViewById(R.id.spin_kit5);
        FadingCircle fadingCircle = new FadingCircle();
        progressBar.setIndeterminateDrawable(fadingCircle);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        userId = user.getUid();
        usuario = firebaseDatabase.getReference();
        incioSesionActivo = new IncioSesionActivo(this);
        HashMap<String, String> user1 = incioSesionActivo.LoggedInUser();
        nombreUsuario = user1.get(incioSesionActivo.USER_NOMBRE);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);


        Perfil();

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

    }

    private void Perfil() {

        usuario.child("USUARIOS").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    String key = usuario.child("USUARIOS").child(userId).getKey();
                    String key2 = snapshot.getKey();
                    if (key.equals(key2)){
                        Usuario usuario = snapshot.getValue(Usuario.class);
                        nombreUsuario1 = usuario.getNombresYApellidos();
                        direccion = usuario.getDireccion();
                        telefono = usuario.getTelefono();
                        setupActionBar();
                    }else {
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(nombreUsuario1);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            firebaseAuth.signOut();
            IncioSesionActivo inicioSesion = new IncioSesionActivo(this);
            SharedPreferences sharedPreferences = getSharedPreferences(inicioSesion.SHARED_PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.commit();
            startActivity(new Intent(this, InicioSesion.class));
            finish();
            return true;
        }

        if(id == R.id.aggReserva){
            agregarReserva();
        }

        return super.onOptionsItemSelected(item);
    }

    private void agregarReserva() {
        final AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
        dialogo.setTitle("Agregar Reservacion");
        dialogo.setCancelable(false);
        LayoutInflater inflater = LayoutInflater.from(this);
        final View reserva = inflater.inflate(R.layout.layout_new_reservacion, null);
        final EditText txtFecha = (EditText) reserva.findViewById(R.id.efecha);
        final EditText txtHora = (EditText) reserva.findViewById(R.id.ehora);
        final EditText txtCodigo = (EditText) reserva.findViewById(R.id.txtCodigo);
        final EditText txtNpersona = (EditText) reserva.findViewById(R.id.txtNpersona);
        final EditText txtObservacion = (EditText) reserva.findViewById(R.id.txtObservacion);

        Random random = new Random();
        final int codigo = random.nextInt(5000);
        txtCodigo.setText(String.valueOf(codigo));




        dialogo.setView(reserva);


        txtFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c= Calendar.getInstance();
                dia=c.get(Calendar.DAY_OF_MONTH);
                mes=c.get(Calendar.MONTH);
                ano=c.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(BarMenu.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        txtFecha.setText(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);
                    }
                }
                        ,ano,mes,dia);
                datePickerDialog.show();
            }
        });

        txtHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c= Calendar.getInstance();
                hora=c.get(Calendar.HOUR_OF_DAY);
                minutos=c.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(BarMenu.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        txtHora.setText(hourOfDay+"H"+minute);
                    }
                },hora,minutos,true);
                timePickerDialog.show();
            }
        });

        dialogo.setPositiveButton("AGREGAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (txtCodigo.getText().toString().isEmpty()) {

                } else if (txtFecha.getText().toString().isEmpty()) {

                } else if (txtHora.getText().toString().isEmpty()) {

                } else if (txtNpersona.getText().toString().isEmpty()) {

                }else if (txtObservacion.getText().toString().isEmpty()) {

                }
                else {
                    progressBar.setVisibility(View.VISIBLE);
                    Reservacion reservacion = new Reservacion();
                    reservacion.setCodigo(String.valueOf(codigo));
                    reservacion.setFecha(txtFecha.getText().toString().trim());
                    reservacion.setHora(txtHora.getText().toString().trim());
                    reservacion.setCantidad(txtNpersona.getText().toString().trim());
                    reservacion.setObservacion(txtObservacion.getText().toString().trim());
                    reservacion.setNombre(nombreUsuario1);
                    reservacion.setTelefono(telefono);
                    reservacion.setDireccion(direccion);
                    reservacion.setCalificacion("0");
                    reservacion.setCalificacionAdmin("0");
                    reservacion.setIdCliente(userId);
                    DatabaseReference reservacionCliente = FirebaseDatabase.getInstance().getReference();

                    reservacionCliente.child("RESERVACIONES").child(String.valueOf(codigo)).setValue(reservacion)
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {

                                                    }
                                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                        }
                    });

                    usuario.child("USUARIOS").child(userId).child("RESERVACIONES").child(String.valueOf(codigo))
                            .setValue(reservacion).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(BarMenu.this, "RESERVACION REGISTRADA", Toast.LENGTH_SHORT).show();

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                        }
                    });
                }

            }
        });

        dialogo.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialogo.show();

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragmento_menu, container, false);
           // TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            //textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //return PlaceholderFragment.newInstance(position + 1);
            switch (position){
                case 0:
                    fragmento_menu fragmento_menu = new fragmento_menu();
                    return fragmento_menu;
                case 1:
                    fragemento_reservacion fragemento_reservacion = new fragemento_reservacion();
                    return fragemento_reservacion;
                case 2:
                    fragmento_perfil fragmento_perfil = new fragmento_perfil();
                    return fragmento_perfil;
            }
            return null;

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

    }

}
