package facci.guaman.conforme.projectapp.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.FadingCircle;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;
import java.util.UUID;

import facci.guaman.conforme.projectapp.Clases.IncioSesionActivo;
import facci.guaman.conforme.projectapp.Fragment.Fragmento_reservacion_admin;
import facci.guaman.conforme.projectapp.Fragment.fragmento_menu;
import facci.guaman.conforme.projectapp.Fragment.fragmento_perfil;
import facci.guaman.conforme.projectapp.Modelo.Menu_Bar;
import facci.guaman.conforme.projectapp.Modelo.Usuario;
import facci.guaman.conforme.projectapp.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PropietarioMenu extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private String userId, nombreUsuario, TipoDB, TipoSared;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private IncioSesionActivo incioSesionActivo;
    private MenuItem AggPlato, AggUsuario;
    private ImageView imageView;
    private Uri imageUri = null;
    private String tipo_user = "";
    private ProgressBar progressBar;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_propietario_menu);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Merriweather-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        userId = user.getUid();
        usuario = firebaseDatabase.getReference();
        incioSesionActivo = new IncioSesionActivo(this);
        HashMap<String, String> user1 = incioSesionActivo.LoggedInUser();
        TipoSared = user1.get(incioSesionActivo.USER_TIPO);
        nombreUsuario = user1.get(incioSesionActivo.USER_NOMBRE);
        progressBar = (ProgressBar)findViewById(R.id.spin_kit4);
        FadingCircle fadingCircle = new FadingCircle();
        progressBar.setIndeterminateDrawable(fadingCircle);

        Perfil();

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        //Codigo para la flecha de retorno
        setupActionBar();
        //Termina
    }

    private void Perfil(){

        usuario.child("USUARIOS").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    String key = usuario.child("USUARIOS").child(userId).getKey();
                    String key2 = snapshot.getKey();
                    if (key.equals(key2)){
                        Usuario usuario = snapshot.getValue(Usuario.class);
                        //nombreUsuario = usuario.getNombresYApellidos();
                        TipoDB = usuario.getTipo();
                    }else {
                    }


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(nombreUsuario);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_propietario_menu, menu);
        AggPlato = menu.findItem(R.id.AggPlato);
        AggUsuario = menu.findItem(R.id.AggUsuario);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            firebaseAuth.signOut();
            IncioSesionActivo inicioSesion = new IncioSesionActivo(this);
            SharedPreferences sharedPreferences = getSharedPreferences(inicioSesion.SHARED_PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.commit();
            startActivity(new Intent(this, InicioSesion.class));
            finish();
            return true;
        }if (id == R.id.AggUsuario){
            Intent intent = new Intent(this, RegistroAdmin.class);
            startActivity(intent);
            finish();
            //AregarUsuario();
            return true;
        }if (id == R.id.AggPlato){
            AgregarPlato();
        }

        return super.onOptionsItemSelected(item);
    }

    private void AgregarPlato() {
        final AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
        dialogo.setTitle("Agregar Plato");
        dialogo.setCancelable(false);
        LayoutInflater inflater = LayoutInflater.from(this);
        View plato = inflater.inflate(R.layout.layout_new_plato, null);
        final EditText txtNombrePlato = (EditText) plato.findViewById(R.id.txtNombrePlato);
        final EditText txtDescripcionPlato = (EditText) plato.findViewById(R.id.txtDescripcion);
        final EditText txtPrecioPlato = (EditText) plato.findViewById(R.id.txtPrecio);
        imageView = (ImageView) plato.findViewById(R.id.ImgProducto);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setMultiTouchEnabled(true)
                        .start(PropietarioMenu.this);
            }
        });

        dialogo.setView(plato);
        dialogo.setPositiveButton("AGREGAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (txtNombrePlato.getText().toString().isEmpty()) {

                } else if (txtDescripcionPlato.getText().toString().isEmpty()) {

                } else if (txtPrecioPlato.getText().toString().isEmpty()) {

                } else if (imageUri == null) {

                }else {
                    progressBar.setVisibility(View.VISIBLE);
                    StorageReference storage = FirebaseStorage.getInstance().getReference();
                    StorageReference file = storage.child("FotoMenu").child(imageUri.getLastPathSegment());
                    file.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        final Uri url = taskSnapshot.getDownloadUrl();
                        String id_menu = UUID.randomUUID().toString();
                        Menu_Bar menu_bar = new Menu_Bar();
                        menu_bar.setNombre(txtNombrePlato.getText().toString().trim());
                        menu_bar.setPrecio(txtPrecioPlato.getText().toString().trim());
                        menu_bar.setDescripcion(txtDescripcionPlato.getText().toString().trim());
                        menu_bar.setUrlFoto(url.toString());
                        menu_bar.setIdMenu(id_menu);
                        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                        databaseReference.child("MENU").child(id_menu).setValue(menu_bar).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(PropietarioMenu.this, "PLATO AGREGAGO CORRECTAMENTE", Toast.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                            }
                        });
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });

            }

            }
        });

        dialogo.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogo.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imageUri = result.getUri();
                imageView.setImageURI(imageUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, result.getError().toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void AregarUsuario() {

        final AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
        dialogo.setTitle("Agregar Usuario");
        dialogo.setCancelable(false);
        LayoutInflater inflater = LayoutInflater.from(this);
        View usuarioNuevo = inflater.inflate(R.layout.layout_registro, null);
        final EditText nombreUserNuevo = (EditText)usuarioNuevo.findViewById(R.id.txtNombreRA);
        final EditText correoUserNuevo = (EditText)usuarioNuevo.findViewById(R.id.txtCorreoRA);
        final EditText telefonoUserNuevo = (EditText)usuarioNuevo.findViewById(R.id.txtTelefonoRA);
        final EditText direccionUserNuevo = (EditText)usuarioNuevo.findViewById(R.id.txtDirecionRA);
        final EditText contraseñaUserNuevo = (EditText)usuarioNuevo.findViewById(R.id.txtClaveRA);
        final RadioButton radioButtonAdmin = (RadioButton) usuarioNuevo.findViewById(R.id.rbAdminRA);
        final RadioButton radioButtonCliente = (RadioButton) usuarioNuevo.findViewById(R.id.rbClienteRA);
        dialogo.setView(usuarioNuevo);

        dialogo.setPositiveButton("REGISTRAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (radioButtonAdmin.isChecked() == true){
                    tipo_user = "Admin";
                }else if (radioButtonCliente.isChecked() == true){
                    tipo_user = "Cliente";
                }

                if (radioButtonAdmin.isChecked() == false && radioButtonCliente.isChecked() == false){
                    Toast.makeText(PropietarioMenu.this, "Debe Seleccionar Un Tipo De Usuario", Toast.LENGTH_SHORT).show();
                }else {
                    if (nombreUserNuevo.getText().toString().isEmpty()) {

                    } else if (telefonoUserNuevo.getText().toString().isEmpty()) {

                    } else if (direccionUserNuevo.getText().toString().isEmpty()) {

                    } else if (correoUserNuevo.getText().toString().isEmpty()) {

                    } else if (contraseñaUserNuevo.getText().toString().isEmpty()) {

                    } else if (contraseñaUserNuevo.getText().toString().length() < 6) {

                    } else {
                        progressBar.setVisibility(View.VISIBLE);
                        final FirebaseAuth firebaseAuthRegistro = FirebaseAuth.getInstance();
                        firebaseAuthRegistro.createUserWithEmailAndPassword(correoUserNuevo.getText().toString().trim(),
                                contraseñaUserNuevo.getText().toString().trim()).addOnCompleteListener(PropietarioMenu.this,
                                new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (!task.isSuccessful()) {
                                            if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                                                Toast.makeText(PropietarioMenu.this, "EL CORREO CON EL QUE SE INTENTA REGISTRAR YA EXISTE", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {

                                            Usuario usuarioNuevo = new Usuario();
                                            usuarioNuevo.setNombresYApellidos(nombreUserNuevo.getText().toString().trim());
                                            usuarioNuevo.setTelefono(telefonoUserNuevo.getText().toString().trim());
                                            usuarioNuevo.setDireccion(direccionUserNuevo.getText().toString().trim());
                                            usuarioNuevo.setCorreo(correoUserNuevo.getText().toString().trim());
                                            usuarioNuevo.setTipo(tipo_user);
                                            DatabaseReference databaseReferenceuserNuevo = FirebaseDatabase.getInstance().getReference("USUARIOS");
                                            databaseReferenceuserNuevo.child(firebaseAuthRegistro.getCurrentUser().getUid()).
                                                    setValue(usuarioNuevo).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    progressBar.setVisibility(View.GONE);
                                                    firebaseAuth.signOut();
                                                    IncioSesionActivo inicioSesion = new IncioSesionActivo(PropietarioMenu.this);
                                                    SharedPreferences sharedPreferences = getSharedPreferences(inicioSesion.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                                    editor.clear();
                                                    editor.commit();
                                                    startActivity(new Intent(PropietarioMenu.this, InicioSesion.class));
                                                    finish();
                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Toast.makeText(PropietarioMenu.this, "ERROR AL REGISTRAR", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                            }
                        });
                    }
                }

            }
        });
        dialogo.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialogo.show();

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragmento_menu, container, false);
//            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
//            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //return PlaceholderFragment.newInstance(position + 1);

            switch (position){
                case 0:
                    fragmento_menu fragmento_menu = new fragmento_menu();
                    return fragmento_menu;
                case 1:
                    Fragmento_reservacion_admin fragmento_reservacion_admin = new Fragmento_reservacion_admin();
                    return fragmento_reservacion_admin;
                case 2:
                    fragmento_perfil fragmento_perfil = new fragmento_perfil();
                    return fragmento_perfil;
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
}
