package facci.guaman.conforme.projectapp.Modelo;

public class Menu_Bar {

    private String Nombre, Descripcion, Precio, urlFoto, idMenu;

    public Menu_Bar() {
    }

    public Menu_Bar(String nombre, String descripcion, String precio, String urlFoto, String idMenu) {
        Nombre = nombre;
        Descripcion = descripcion;
        Precio = precio;
        this.urlFoto = urlFoto;
        this.idMenu = idMenu;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getPrecio() {
        return Precio;
    }

    public void setPrecio(String precio) {
        Precio = precio;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(String idMenu) {
        this.idMenu = idMenu;
    }
}
