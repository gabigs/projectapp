package facci.guaman.conforme.projectapp.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.guaman.conforme.projectapp.Clases.IncioSesionActivo;
import facci.guaman.conforme.projectapp.Modelo.Usuario;
import facci.guaman.conforme.projectapp.R;

public class RegistroAdmin extends AppCompatActivity  implements View.OnClickListener {

    Button btnCancelar,btnRegistrar;
    EditText nombreUserNuevo, correoUserNuevo,telefonoUserNuevo,direccionUserNuevo,contraseñaUserNuevo;
    RadioButton radioButtonAdmin,radioButtonCliente;
    private FirebaseAuth firebaseAuth;

    String tipo_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_admin);

        btnCancelar = (Button) findViewById(R.id.btnCancelar);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrarAdmin);

        nombreUserNuevo = (EditText) findViewById(R.id.txtNombreRA);
        correoUserNuevo = (EditText) findViewById(R.id.txtCorreoRA);
        telefonoUserNuevo = (EditText) findViewById(R.id.txtTelefonoRA);
        direccionUserNuevo = (EditText) findViewById(R.id.txtDirecionRA);
        contraseñaUserNuevo = (EditText) findViewById(R.id.txtClaveRA);
        radioButtonAdmin = (RadioButton) findViewById(R.id.rbAdminRA);
        radioButtonCliente = (RadioButton) findViewById(R.id.rbClienteRA);

        firebaseAuth = FirebaseAuth.getInstance();

        btnCancelar.setOnClickListener(this);
        btnRegistrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnCancelar:
                Intent intent = new Intent(this, PropietarioMenu.class);
                startActivity(intent);
                finish();
                break;
            case R.id.btnRegistrarAdmin:
                registroNuevo();
                break;
        }
    }

    private void registroNuevo(){
        if (radioButtonAdmin.isChecked() == true){
            tipo_user = "Admin";
        }else if (radioButtonCliente.isChecked() == true){
            tipo_user = "Cliente";
        }
        if (radioButtonAdmin.isChecked() == false && radioButtonCliente.isChecked() == false){
            Toast.makeText(RegistroAdmin.this, "Debe Seleccionar Un Tipo De Usuario", Toast.LENGTH_SHORT).show();
        }else {
            if (nombreUserNuevo.getText().toString().isEmpty()) {

            } else if (telefonoUserNuevo.getText().toString().isEmpty()) {

            } else if (direccionUserNuevo.getText().toString().isEmpty()) {

            } else if (correoUserNuevo.getText().toString().isEmpty()) {

            } else if (contraseñaUserNuevo.getText().toString().isEmpty()) {

            } else if (contraseñaUserNuevo.getText().toString().length() < 6) {

            } else {
               // progressBar.setVisibility(View.VISIBLE);
                final FirebaseAuth firebaseAuthRegistro = FirebaseAuth.getInstance();
                firebaseAuthRegistro.createUserWithEmailAndPassword(correoUserNuevo.getText().toString().trim(),
                        contraseñaUserNuevo.getText().toString().trim()).addOnCompleteListener(RegistroAdmin.this,
                        new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (!task.isSuccessful()) {
                                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                                        Toast.makeText(RegistroAdmin.this, "EL CORREO CON EL QUE SE INTENTA REGISTRAR YA EXISTE", Toast.LENGTH_SHORT).show();
                                    }
                                } else {

                                    Usuario usuarioNuevo = new Usuario();
                                    usuarioNuevo.setNombresYApellidos(nombreUserNuevo.getText().toString().trim());
                                    usuarioNuevo.setTelefono(telefonoUserNuevo.getText().toString().trim());
                                    usuarioNuevo.setDireccion(direccionUserNuevo.getText().toString().trim());
                                    usuarioNuevo.setCorreo(correoUserNuevo.getText().toString().trim());
                                    usuarioNuevo.setTipo(tipo_user);
                                    DatabaseReference databaseReferenceuserNuevo = FirebaseDatabase.getInstance().getReference("USUARIOS");
                                    databaseReferenceuserNuevo.child(firebaseAuthRegistro.getCurrentUser().getUid()).
                                            setValue(usuarioNuevo).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                           // progressBar.setVisibility(View.GONE);
                                            firebaseAuth.signOut();
                                            IncioSesionActivo inicioSesion = new IncioSesionActivo(RegistroAdmin.this);
                                            SharedPreferences sharedPreferences = getSharedPreferences(inicioSesion.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = sharedPreferences.edit();
                                            editor.clear();
                                            editor.commit();
                                            startActivity(new Intent(RegistroAdmin.this, InicioSesion.class));
                                            finish();
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(RegistroAdmin.this, "ERROR AL REGISTRAR", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
            }
        }
    }


}
