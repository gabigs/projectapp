package facci.guaman.conforme.projectapp.Modelo;

public class Usuario {

    private String NombresYApellidos, direccion, telefono, correo, tipo;


    public Usuario() {
    }

    public Usuario(String nombresYApellidos, String direccion, String telefono, String correo, String tipo) {
        NombresYApellidos = nombresYApellidos;
        this.direccion = direccion;
        this.telefono = telefono;
        this.correo = correo;
        this.tipo = tipo;
    }

    public String getNombresYApellidos() {
        return NombresYApellidos;
    }

    public void setNombresYApellidos(String nombresYApellidos) {
        NombresYApellidos = nombresYApellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
