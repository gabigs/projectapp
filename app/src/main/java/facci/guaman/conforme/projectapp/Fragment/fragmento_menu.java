package facci.guaman.conforme.projectapp.Fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import facci.guaman.conforme.projectapp.Activity.MenuDetalle;
import facci.guaman.conforme.projectapp.Clases.IncioSesionActivo;
import facci.guaman.conforme.projectapp.Modelo.Menu_Bar;
import facci.guaman.conforme.projectapp.R;

public class fragmento_menu extends Fragment {

    private RecyclerView recyclerView;
    private DatabaseReference databaseReference;
    private FirebaseRecyclerAdapter<Menu_Bar, ViewHolder> adapter;
    private String tipo;
    private IncioSesionActivo incioSesionActivo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragmento_menu, container, false);
        incioSesionActivo = new IncioSesionActivo(getContext());
        HashMap<String, String> user1 = incioSesionActivo.LoggedInUser();
        tipo = user1.get(incioSesionActivo.USER_TIPO);
        recyclerView = (RecyclerView) view.findViewById(R.id.ListViewMenu);
        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        databaseReference = FirebaseDatabase.getInstance().getReference().child("MENU");
        databaseReference.keepSynced(true);

        DatabaseReference productosRef = FirebaseDatabase.getInstance().getReference().child("MENU");
        Query productQuery = productosRef.orderByKey();

        FirebaseRecyclerOptions productOptions = new FirebaseRecyclerOptions.Builder<Menu_Bar>().setQuery(productQuery, Menu_Bar.class).build();

        adapter = new FirebaseRecyclerAdapter<Menu_Bar, ViewHolder>(productOptions) {
            @Override
            protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull final Menu_Bar model) {

                holder.setDescripcion(model.getNombre());
                holder.setFoto(getContext(), model.getUrlFoto());

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), MenuDetalle.class);
                        intent.putExtra("Nombre", model.getNombre());
                        intent.putExtra("Descripcion", model.getDescripcion());
                        intent.putExtra("Precio", model.getPrecio());
                        intent.putExtra("urlFoto", model.getUrlFoto());
                        intent.putExtra("idMenu", model.getIdMenu());
                        intent.putExtra("tipoUser", tipo);
                        startActivity(intent);
                    }
                });
            }

            @NonNull
            @Override
            public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_menu, parent, false);
                return new fragmento_menu.ViewHolder(view);
            }
        };
        recyclerView.setAdapter(adapter);
        return view;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        View mView;
        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setDescripcion(String descripcion) {
            TextView nombreMenu = (TextView)mView.findViewById(R.id.nombreMenu);
            nombreMenu.setText(descripcion);
        }


        public void setFoto(Context context, String foto) {
            ImageView circleImageView = (ImageView) mView.findViewById(R.id.fotoMenu);
            Picasso.with(context).load(foto).into(circleImageView);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();

    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

}
