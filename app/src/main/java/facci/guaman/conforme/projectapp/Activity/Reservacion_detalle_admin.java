package facci.guaman.conforme.projectapp.Activity;


import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;


import com.github.ybq.android.spinkit.style.FadingCircle;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import facci.guaman.conforme.projectapp.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Reservacion_detalle_admin extends AppCompatActivity {

    private EditText codigo, fecha, hora, personas, observacion, nombre, telefono, direcicion;
    private TextView titulo, subTituloCliente, subTituloAdmin, subTituloDatoCliente;
    private Button calificar;
    private RatingBar ratingBar, ratingBarAdmin;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference, usuario, databaseReferenceR;
    private String userId, codigoReserva,merriweatherBold, merriweatherBoldItalic;
    private ProgressBar progressBar;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservacion_detalle);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Merriweather-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        setTitle("Detalle reservacion");

        merriweatherBold = "fonts/Merriweather-Bold.ttf";
        merriweatherBoldItalic = "fonts/Merriweather-BoldItalic.ttf";

        titulo = (TextView) findViewById(R.id.tituloDetalleReserva);
        subTituloCliente = (TextView) findViewById(R.id.subTituloCliente);
        subTituloAdmin = (TextView) findViewById(R.id.subTituloAdmin);
        subTituloDatoCliente = (TextView) findViewById(R.id.subTituloDatoCliente);
        calificar = (Button) findViewById(R.id.btnCalificarAdmin);

        titulo.setTypeface(Typeface.createFromAsset(this.getAssets(), merriweatherBold));
        subTituloCliente.setTypeface(Typeface.createFromAsset(this.getAssets(), merriweatherBold));
        subTituloAdmin.setTypeface(Typeface.createFromAsset(this.getAssets(), merriweatherBold));
        subTituloDatoCliente.setTypeface(Typeface.createFromAsset(this.getAssets(), merriweatherBold));
        calificar.setTypeface(Typeface.createFromAsset(this.getAssets(), merriweatherBoldItalic));

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("USUARIOS");
        databaseReferenceR = firebaseDatabase.getReference("RESERVACIONES");

        userId = getIntent().getStringExtra("idCliente");

        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingBarAdmin = (RatingBar) findViewById(R.id.ratingBarAdmin);
        progressBar = (ProgressBar)findViewById(R.id.spin_kit2);
        FadingCircle fadingCircle = new FadingCircle();
        //progressBar.setIndeterminateDrawable(fadingCircle);

        if(getIntent().getStringExtra("caliAdmin").equals("0")){
            calificar.setVisibility(View.VISIBLE);
            ratingBarAdmin.setVisibility(View.GONE);
        }else{
            calificar.setVisibility(View.GONE);
            ratingBarAdmin.setVisibility(View.VISIBLE);
        }

        codigoReserva = getIntent().getStringExtra("codigo");

        codigo = (EditText) findViewById(R.id.txtCodigoReservacion);
        fecha = (EditText) findViewById(R.id.txtFechaReservacion);
        hora = (EditText) findViewById(R.id.txtHoraReservacion);
        personas = (EditText) findViewById(R.id.txtPersonaReservacion);
        observacion = (EditText) findViewById(R.id.txtOnservacionReservacion);
        nombre = (EditText) findViewById(R.id.txtNombreReservacion);
        telefono = (EditText) findViewById(R.id.txtTelefenoReservacion);
        direcicion = (EditText) findViewById(R.id.txtDireccionReservacion);

        codigo.setText(getIntent().getStringExtra("codigo"));
        fecha.setText(getIntent().getStringExtra("fecha"));
        hora.setText(getIntent().getStringExtra("hora"));
        personas.setText(getIntent().getStringExtra("persona"));
        observacion.setText(getIntent().getStringExtra("observacion"));
        nombre.setText(getIntent().getStringExtra("nombre"));
        telefono.setText(getIntent().getStringExtra("telefono"));
        direcicion.setText(getIntent().getStringExtra("direccion"));

        ratingBar.setRating(Float.parseFloat(getIntent().getStringExtra("calificacion")));
        ratingBarAdmin.setRating(Float.parseFloat(getIntent().getStringExtra("caliAdmin")));


        calificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calificarAdmin();
            }
        });
    }

    private void calificarAdmin(){


        final AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
        dialogo.setTitle("Califique al Cliente");
        dialogo.setCancelable(false);
        LayoutInflater inflater = LayoutInflater.from(this);
        View calificacion = inflater.inflate(R.layout.layout_new_calificacion, null);
        final RatingBar ratingBar = calificacion.findViewById(R.id.ratingBarGeneral);
        dialogo.setView(calificacion);
        dialogo.setPositiveButton("Calificar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
             //   progressBar.setVisibility(View.VISIBLE);
                String cali = String.valueOf(ratingBar.getRating());
                HashMap<String, Object> objectHashMap = new HashMap<>();
                objectHashMap.put("calificacionAdmin", cali);
                databaseReference.child(userId).child("RESERVACIONES").child(codigoReserva).updateChildren(objectHashMap);
                databaseReferenceR.child(codigoReserva).updateChildren(objectHashMap);
                finish();
            }
        });

        dialogo.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialogo.show();
    }
}
