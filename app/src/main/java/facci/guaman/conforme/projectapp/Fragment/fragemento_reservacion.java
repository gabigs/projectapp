package facci.guaman.conforme.projectapp.Fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import facci.guaman.conforme.projectapp.Activity.MenuDetalle;
import facci.guaman.conforme.projectapp.Activity.Reservacion_detalle_cliente;
import facci.guaman.conforme.projectapp.Modelo.Menu_Bar;
import facci.guaman.conforme.projectapp.Modelo.Reservacion;
import facci.guaman.conforme.projectapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class fragemento_reservacion extends Fragment {

    private RecyclerView recyclerView;
    private DatabaseReference databaseReference;
    private FirebaseRecyclerAdapter<Reservacion, fragemento_reservacion.ViewHolder> adapter;
    private String userId;
    private FirebaseAuth firebaseAuth;


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragemento_reservacion, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.ListViewReservacion);
        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        userId = user.getUid();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("USUARIOS").child(userId).child("RESERVACIONES");
        databaseReference.keepSynced(true);

        DatabaseReference productosRef = FirebaseDatabase.getInstance().getReference().child("USUARIOS").child(userId).child("RESERVACIONES");
        Query productQuery = productosRef.orderByKey();

        FirebaseRecyclerOptions productOptions = new FirebaseRecyclerOptions.Builder<Reservacion>().setQuery(productQuery, Reservacion.class).build();

        adapter = new FirebaseRecyclerAdapter<Reservacion, ViewHolder>(productOptions) {
            @Override
            protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull final Reservacion model) {
                holder.setNombreR(model.getNombre());
                holder.setFecha(model.getFecha() + " " + model.getHora());
                holder.setTelefono(model.getTelefono());

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), Reservacion_detalle_cliente.class);
                        intent.putExtra("codigo", model.getCodigo());
                        intent.putExtra("fecha", model.getFecha());
                        intent.putExtra("hora", model.getHora());
                        intent.putExtra("persona", model.getCantidad());
                        intent.putExtra("observacion", model.getObservacion());
                        intent.putExtra("tipo","Cliente");
                        intent.putExtra("calificacion", model.getCalificacion());
                        intent.putExtra("caliAdmin", model.getCalificacionAdmin());
                        startActivity(intent);
                    }
                });

            }

            @NonNull
            @Override
            public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_item_reservacion, viewGroup, false);
                return new fragemento_reservacion.ViewHolder(view);
            }
        };

        recyclerView.setAdapter(adapter);

        return view;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        View mView;
        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setNombreR(String nombre) {
            TextView nombreUsuario = (TextView) mView.findViewById(R.id.nombreCliente);
            nombreUsuario.setText(nombre);
        }
        public void setTelefono(String nombre) {
            TextView nombreUsuario = (TextView) mView.findViewById(R.id.telefonoCliente);
            nombreUsuario.setText(nombre);

        }public void setFecha(String nombre) {
            TextView nombreUsuario = (TextView) mView.findViewById(R.id.fechaReservacion);
            nombreUsuario.setText(nombre);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();

    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

}
