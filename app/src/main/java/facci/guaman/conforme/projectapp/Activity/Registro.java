package facci.guaman.conforme.projectapp.Activity;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.FadingCircle;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import facci.guaman.conforme.projectapp.Modelo.Usuario;
import facci.guaman.conforme.projectapp.R;

public class Registro extends AppCompatActivity implements View.OnClickListener{

    EditText txtNombre, txtDireccion, txtTelefono, txtCorreoR, txtClaveR;
    Button btnR;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);


        //Codigo para la flecha de retorno
        setupActionBar();

        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtDireccion = (EditText) findViewById(R.id.txtDirecion);
        txtTelefono = (EditText) findViewById(R.id.txtTelefono);
        txtCorreoR = (EditText) findViewById(R.id.txtCorreoR);
        txtClaveR = (EditText) findViewById(R.id.txtClaveR);
        btnR = (Button) findViewById(R.id.btnR);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("USUARIOS");
        progressBar = (ProgressBar)findViewById(R.id.spin_kit1);
        FadingCircle fadingCircle = new FadingCircle();
        progressBar.setIndeterminateDrawable(fadingCircle);
        btnR.setOnClickListener(this);
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Registrarse");
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnR:

                if (txtNombre.getText().toString().isEmpty()){
                    txtNombre.setError("ESCRIBA SUS NOMBRES Y APELLIDOS");
                }else if (txtDireccion.getText().toString().isEmpty()){
                    txtDireccion.setError("ESCRIBA SU DIRECCION");
                }else if (txtTelefono.getText().toString().isEmpty()){
                    txtTelefono.setError("ESCRIBA SU NUMERO TELEFONICO");
                }else if (txtClaveR.getText().toString().isEmpty()){
                    txtClaveR.setError("ESCRIBA UNA CLAVE");
                }else if (txtCorreoR.getText().toString().isEmpty()){
                    txtCorreoR.setError("ESCRIBA UN CORREO ELECTRONICO");
                }else if (txtClaveR.getText().toString().length() < 6){
                   txtClaveR.setError("SU CLAVE DEBE TENER UNA LONGITUD MINIMA DE SEIS");
                }else {

                progressBar.setVisibility(View.VISIBLE);
                firebaseAuth.createUserWithEmailAndPassword(txtCorreoR.getText().toString().trim(),
                        txtClaveR.getText().toString().trim()).addOnCompleteListener(this,
                        new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                if (!task.isSuccessful()) {

                                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                                        progressBar.setVisibility(View.GONE);
                                        Toast.makeText(Registro.this, "EL CORREO CON EL QUE SE INTENTA REGISTRAR YA EXISTE", Toast.LENGTH_SHORT).show();
                                    }
                                }else {
                                    Usuario usuario = new Usuario();
                                    usuario.setNombresYApellidos(txtNombre.getText().toString().trim());
                                    usuario.setDireccion(txtDireccion.getText().toString().trim());
                                    usuario.setTelefono(txtTelefono.getText().toString().trim());
                                    usuario.setCorreo(txtCorreoR.getText().toString().trim());
                                    usuario.setTipo("Cliente");
                                    databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(usuario)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    progressBar.setVisibility(View.GONE);
                                                    finish();
                                                    startActivity(new Intent(Registro.this, InicioSesion.class));
                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {

                                        }
                                    });
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
                }

                break;
        }

    }
}
