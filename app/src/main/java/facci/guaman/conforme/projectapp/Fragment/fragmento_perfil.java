package facci.guaman.conforme.projectapp.Fragment;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.HashMap;

import facci.guaman.conforme.projectapp.Modelo.Usuario;
import facci.guaman.conforme.projectapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class fragmento_perfil extends Fragment implements View.OnClickListener{


    private EditText nombreCompleto, direccion, telefono, correo, tipo;
    private Button btnEditar, btnModificar;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private DatabaseReference databaseReference;
    private String userId, merriweatherBold, merriweatherBoldItalic;
    private ProgressBar progressBar;
    private TextView tituloPeril;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragmento_perfil, container, false);

        merriweatherBold = "fonts/Merriweather-Bold.ttf";
        merriweatherBoldItalic = "fonts/Merriweather-BoldItalic.ttf";

        tituloPeril = (TextView) view.findViewById(R.id.tituloPerfil);
        btnEditar = (Button) view.findViewById(R.id.btnEditar);
        btnModificar = (Button) view.findViewById(R.id.btnModificar);

        tituloPeril.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), merriweatherBold));
        btnEditar.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), merriweatherBoldItalic));
        btnModificar.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), merriweatherBoldItalic));

        nombreCompleto = (EditText) view.findViewById(R.id.txtNombreP);
        direccion = (EditText) view.findViewById(R.id.txtDirecionP);
        telefono = (EditText) view.findViewById(R.id.txtTelefonoP);
        correo = (EditText) view.findViewById(R.id.txtCorreoP);
        tipo = (EditText) view.findViewById(R.id.txtTipoP);
        progressBar = (ProgressBar)view.findViewById(R.id.Progress);
        nombreCompleto.setEnabled(false);
        direccion.setEnabled(false);
        telefono.setEnabled(false);
        correo.setEnabled(false);
        tipo.setEnabled(false);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        userId = user.getUid();
        usuario = firebaseDatabase.getReference();
        databaseReference = firebaseDatabase.getReference("USUARIOS");
        Perfil();
        btnEditar.setOnClickListener(this);
        btnModificar.setOnClickListener(this);

        return view;
    }

    private void Perfil() {

        usuario.child("USUARIOS").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    String key = usuario.child("USUARIOS").child(userId).getKey();
                    String key2 = snapshot.getKey();
                    if (key.equals(key2)){
                        Usuario usuario = snapshot.getValue(Usuario.class);
                        nombreCompleto.setText(usuario.getNombresYApellidos());
                        direccion.setText(usuario.getDireccion());
                        telefono.setText(usuario.getTelefono());
                        correo.setText(usuario.getCorreo());
                        tipo.setText(usuario.getTipo());
                    }else {

                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnEditar:
                nombreCompleto.setEnabled(true);
                direccion.setEnabled(true);
                telefono.setEnabled(true);
                btnEditar.setVisibility(View.GONE);
                btnModificar.setVisibility(View.VISIBLE);
                break;

            case R.id.btnModificar:
                progressBar.setVisibility(View.VISIBLE);

                HashMap<String, Object> objectHashMap = new HashMap<>();
                objectHashMap.put("nombresYApellidos", nombreCompleto.getText().toString().trim());
                objectHashMap.put("direccion", direccion.getText().toString().trim());
                objectHashMap.put("telefono", telefono.getText().toString().trim());
                databaseReference.child(userId).updateChildren(objectHashMap);
                progressBar.setVisibility(View.GONE);
                nombreCompleto.setEnabled(false);
                direccion.setEnabled(false);
                telefono.setEnabled(false);
                correo.setEnabled(false);
                tipo.setEnabled(false);
                btnEditar.setVisibility(View.VISIBLE);
                btnModificar.setVisibility(View.GONE);
                break;
        }

    }
}
