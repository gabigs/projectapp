package facci.guaman.conforme.projectapp.Activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import facci.guaman.conforme.projectapp.R;

public class Icono extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_icono);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Icono.this, InicioSesion.class);
                startActivity(intent);

                finish();
            }
        },1000);

    }
}
