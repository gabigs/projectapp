package facci.guaman.conforme.projectapp.Clases;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class IncioSesionActivo {

    public static final String SHARED_PREF_NAME = "BarLogin";
    public static final String USER_ID = "id_user";
    public static final String USER_TIPO = "tipo_user";
    public static final String USER_NOMBRE = "nombre_user";
    public static IncioSesionActivo incioSesionActivo;
    public static Context mCtx;

    public IncioSesionActivo(Context context) {
        mCtx = context;
    }

    public static synchronized IncioSesionActivo getInstance(Context context) {
        if (incioSesionActivo == null) {
            incioSesionActivo = new IncioSesionActivo(context);
        }
        return incioSesionActivo;
    }

    public void storeUserName(String id, String tipo, String nombre) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_ID, id);
        editor.putString(USER_TIPO, tipo);
        editor.putString(USER_NOMBRE, nombre);
        editor.commit();
    }

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_ID, null) != null;
    }

    public HashMap<String, String> LoggedInUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        HashMap<String, String> usuario = new HashMap<>();
        usuario.put(USER_ID, sharedPreferences.getString(USER_ID, null));
        usuario.put(USER_TIPO, sharedPreferences.getString(USER_TIPO, null));
        usuario.put(USER_NOMBRE, sharedPreferences.getString(USER_NOMBRE, null));
        return usuario;

    }

}
