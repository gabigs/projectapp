package facci.guaman.conforme.projectapp.Activity;

import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.FitWindowsFrameLayout;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.github.ybq.android.spinkit.style.FadingCircle;
import com.github.ybq.android.spinkit.style.Wave;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import facci.guaman.conforme.projectapp.Clases.IncioSesionActivo;
import facci.guaman.conforme.projectapp.Modelo.Usuario;
import facci.guaman.conforme.projectapp.R;

public class InicioSesion extends AppCompatActivity implements View.OnClickListener{

    private EditText txtCorreo, txtClave;
    private TextView tvRegistrarse;
    private Button btnIniciarSecion;
    private RadioButton rbCliente, rbAdmin;
    private IncioSesionActivo incioSesionActivo;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    private ObjectAnimator anim;
    private LinearLayout login;
    private String tipo;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_sesion);

        incioSesionActivo = new IncioSesionActivo(this);
        if (incioSesionActivo.isLoggedIn()){
            HashMap<String, String> user = incioSesionActivo.LoggedInUser();
            String tipo = user.get(incioSesionActivo.USER_TIPO);
            if (tipo.equals("Admin")){
                startActivity(new Intent(this, PropietarioMenu.class));
                finish();
            }else if (tipo.equals("Cliente")){
                startActivity(new Intent(this, BarMenu.class));
                finish();
            }
        }

        progressBar = (ProgressBar)findViewById(R.id.spin_kit);
        FadingCircle fadingCircle = new FadingCircle();
        progressBar.setIndeterminateDrawable(fadingCircle);

        txtCorreo = (EditText) findViewById(R.id.txtUsuario);
        txtClave = (EditText) findViewById(R.id.txtClave);

        tvRegistrarse = (TextView) findViewById(R.id.tvRegistro);

        btnIniciarSecion = (Button) findViewById(R.id.btnIniciarSesion);
        rbCliente = (RadioButton) findViewById(R.id.rbCliente);
        rbAdmin = (RadioButton) findViewById(R.id.rbAdmin);
        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);

        btnIniciarSecion.setOnClickListener(this);
        tvRegistrarse.setOnClickListener(this);

        login = (LinearLayout) findViewById(R.id.login);

        //miprogress = (ProgressBar) findViewById(R.id.circularProgress);


    }

    private void mostrarProgress(){
        login.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

    }
    private void ocultarProgress(){
        progressBar.setVisibility(View.GONE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnIniciarSesion:
                if (txtCorreo.getText().toString().isEmpty()){
                    txtCorreo.setError("ESCRIBA SU CORREO");
                }else if (txtClave.getText().toString().isEmpty()){
                    txtClave.setError("ESCRIBA SU CLAVE");
                }else if (rbAdmin.isChecked()==false && rbCliente.isChecked()==false) {
                    Toast.makeText(this, "DEBE SELECCIONAR SU TIPO DE USUARIO", Toast.LENGTH_SHORT).show();
                }else {
                    mostrarProgress();
                    firebaseAuth.signInWithEmailAndPassword(txtCorreo.getText().toString().trim(), txtClave.getText().toString().trim())
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()){
                                        //progressDialog.dismiss();
                                        final String idprueba = FirebaseAuth.getInstance().getCurrentUser().getUid();
                                        if (rbCliente.isChecked() == true){
                                            firebaseDatabase = FirebaseDatabase.getInstance();
                                            usuario = firebaseDatabase.getReference();
                                            usuario.child("USUARIOS").addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
                                                        String key = usuario.child("USUARIOS").child(idprueba).getKey();
                                                        String key2 = snapshot.getKey();
                                                        if (key.equals(key2)){
                                                            Usuario usuario = snapshot.getValue(Usuario.class);
                                                            String nombreUsuario = usuario.getNombresYApellidos();
                                                            String TipoDB = usuario.getTipo();
                                                            if (TipoDB.equals("Admin")){
                                                                ocultarProgress();
                                                                login.setVisibility(View.VISIBLE);
                                                                firebaseAuth.signOut();
                                                                Toast.makeText(InicioSesion.this, "SU TIPO DE USUARIO NO ES CORRECTO", Toast.LENGTH_SHORT).show();
                                                            }else {
                                                                ocultarProgress();
                                                                IncioSesionActivo.getInstance(InicioSesion.this).storeUserName(FirebaseAuth.getInstance().getUid(), "Cliente", nombreUsuario);
                                                                startActivity(new Intent(getApplication(), BarMenu.class));
                                                                finish();
                                                            }
                                                        }else {

                                                        }
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });

                                        }else if (rbAdmin.isChecked() == true){
                                            firebaseDatabase = FirebaseDatabase.getInstance();
                                            usuario = firebaseDatabase.getReference();
                                            usuario.child("USUARIOS").addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
                                                        String key = usuario.child("USUARIOS").child(idprueba).getKey();
                                                        String key2 = snapshot.getKey();
                                                        if (key.equals(key2)){
                                                            Usuario usuario = snapshot.getValue(Usuario.class);
                                                            String nombreUsuario = usuario.getNombresYApellidos();
                                                            String TipoDB = usuario.getTipo();
                                                            Log.e("A", TipoDB);
                                                            if (TipoDB.equals("Cliente")){
                                                                ocultarProgress();
                                                                login.setVisibility(View.VISIBLE);
                                                                firebaseAuth.signOut();
                                                                Toast.makeText(InicioSesion.this, "SU TIPO DE USUARIO NO ES CORRECTO", Toast.LENGTH_SHORT).show();
                                                            }else {
                                                                ocultarProgress();
                                                                IncioSesionActivo.getInstance(InicioSesion.this).storeUserName(FirebaseAuth.getInstance().getUid(), "Admin", nombreUsuario);
                                                                startActivity(new Intent(getApplication(), PropietarioMenu.class));
                                                                finish();
                                                            }
                                                        }else {
                                                        }
                                                    }
                                                }
                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });
                                        }
                                    }else {
                                        login.setVisibility(View.VISIBLE);
                                        ocultarProgress();
                                        if (task.getException() instanceof FirebaseAuthUserCollisionException) {//si se presenta una colisiÛn
                                            Toast.makeText(InicioSesion.this, "Ese usuario ya existe", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(InicioSesion.this, "El usuario o contraseña son incorectos...!! ", Toast.LENGTH_LONG).show();
                                            txtClave.setText("");
                                            txtCorreo.setText("");
                                            txtCorreo.requestFocus();
                                        }

                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                        }
                    });
                }


                break;
            case R.id.tvRegistro:
                startActivity(new Intent(this, Registro.class));
                break;
        }
    }
}
