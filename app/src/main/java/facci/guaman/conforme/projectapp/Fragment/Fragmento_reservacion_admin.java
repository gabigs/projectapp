package facci.guaman.conforme.projectapp.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.zip.Inflater;

import facci.guaman.conforme.projectapp.Activity.Reservacion_detalle_admin;
import facci.guaman.conforme.projectapp.Activity.Reservacion_detalle_cliente;
import facci.guaman.conforme.projectapp.Modelo.Reservacion;
import facci.guaman.conforme.projectapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragmento_reservacion_admin extends Fragment {

    private RecyclerView recyclerView;
    private DatabaseReference databaseReference;
    private FirebaseRecyclerAdapter<Reservacion, Fragmento_reservacion_admin.ViewHolder> adapter;


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragmento_reservacion_admin, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.ListViewReservacionAdmin);
        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        databaseReference = FirebaseDatabase.getInstance().getReference().child("RESERVACIONES");
        databaseReference.keepSynced(true);

        DatabaseReference productosRef = FirebaseDatabase.getInstance().getReference().child("RESERVACIONES");
        Query productQuery = productosRef.orderByKey();

        FirebaseRecyclerOptions productOptions = new FirebaseRecyclerOptions.Builder<Reservacion>().setQuery(productQuery, Reservacion.class).build();

        adapter = new FirebaseRecyclerAdapter<Reservacion, ViewHolder>(productOptions) {
            @Override
            protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull final Reservacion model) {
                holder.setNombreR(model.getNombre());
                holder.setFecha(model.getFecha() + " " + model.getHora());
                holder.setTelefono(model.getTelefono());

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), Reservacion_detalle_admin.class);
                        intent.putExtra("codigo", model.getCodigo());
                        intent.putExtra("fecha", model.getFecha());
                        intent.putExtra("hora", model.getHora());
                        intent.putExtra("persona", model.getCantidad());
                        intent.putExtra("observacion", model.getObservacion());
                        intent.putExtra("tipo","Admin");
                        intent.putExtra("calificacion", model.getCalificacion());
                        intent.putExtra("caliAdmin", model.getCalificacionAdmin());
                        intent.putExtra("idCliente", model.getIdCliente());
                        intent.putExtra("nombre", model.getNombre());
                        intent.putExtra("telefono", model.getTelefono());
                        intent.putExtra("direccion", model.getDireccion());

                        startActivity(intent);
                    }
                });

            }

            @NonNull
            @Override
            public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_item_reservacion, viewGroup, false);
                return new Fragmento_reservacion_admin.ViewHolder(view1);
            }
        };

        recyclerView.setAdapter(adapter);

        return view;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        View mView;
        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setNombreR(String nombre) {
            TextView nombreUsuario = (TextView) mView.findViewById(R.id.nombreCliente);
            nombreUsuario.setText(nombre);
        }
        public void setTelefono(String nombre) {
            TextView nombreUsuario = (TextView) mView.findViewById(R.id.telefonoCliente);
            nombreUsuario.setText(nombre);

        }public void setFecha(String nombre) {
            TextView nombreUsuario = (TextView) mView.findViewById(R.id.fechaReservacion);
            nombreUsuario.setText(nombre);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();

    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

}
